import { required, minLength } from 'vuelidate/lib/validators'

const customValidator = value => {
  if (typeof value === 'undefined' || value === null || value === '') {
    return true
  }
  return /^[a-z\d\-_\s]+$/i.test(value)
}

export const validation = {
  data () {
    return {
      name: '',
      submitStatus: null,
      description: ''
    }
  },
  validations: {
    name: {
      required,
      minLength: minLength(4)
    },
    description: {
      required,
      minLength: minLength(10),
      customValidator
    }
  },
  methods: {
    submit () {
      this.$v.$touch()
      if (this.$v.$invalid) {
        this.submitStatus = 'ERROR'
      } else {
        this.submitStatus = 'PENDING'
        setTimeout(() => {
          this.submitStatus = 'OK'
          this.$store.commit('createName', this.$v.name.$model)
          this.$store.commit('createDescription', this.$v.description.$model)
          this.$store.commit('addTaskVerify', this.submitStatus)
        }, 500)
      }
    },
    update () {
      console.log(this.getTaskId)
      this.$v.$touch()
      if (this.$v.$invalid) {
        this.submitStatus = 'ERROR'
      } else {
        this.submitStatus = 'PENDING'
        setTimeout(() => {
          this.submitStatus = 'OK'
          this.$route.params['task_name'] = this.getTaskId
          this.$store.commit('createName', this.$v.name.$model)
          this.$store.commit('createDescription', this.$v.description.$model)
          this.$store.commit('editTaskRow', this.getTaskId)
        }, 500)
      }
    }
  }
}
