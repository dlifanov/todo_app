import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tasks: []
  },
  plugins: [createPersistedState()],
  mutations: {
    createDescription (state, description) {
      state.tasks.description = description
    },
    createName (state, name) {
      state.tasks.name = name
    },
    updateId (state, id) {
      state.tasks.id = id
    },
    createDeadline (state, deadline) {
      state.tasks.deadline = deadline
    },
    // addTask (state) {
    //   state.tasks.push({
    //     name: state.tasks.name,
    //     description: state.tasks.description,
    //     deadline: state.tasks.deadline,
    //     status: 'In Progress'
    //   })
    //   // location.assign('/')
    // },
    addTaskVerify (state, submitStatus) {
      if (submitStatus === 'OK') {
        state.tasks.push({
          name: state.tasks.name,
          description: state.tasks.description,
          deadline: state.tasks.deadline,
          status: 'In Progress'
        })
        location.assign('/')
      }
    },
    doneStatus (state, id, submitStatus) {
      state.tasks[id]['status'] = 'Done'
      location.assign('/')
    },
    editTaskRow (state, id) {
      state.tasks[id]['name'] = state.tasks.name
      state.tasks[id]['description'] = state.tasks.description
      state.tasks[id]['deadline'] = state.tasks.deadline
      location.assign('/')
    }
  }
})
