import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/AllTasks.vue')
    },
    {
      path: '/create',
      name: 'create',
      component: () => import('./views/CreateTask.vue')
    },
    {
      path: '/edit/:task_name',
      name: 'edit',
      props: true,
      component: () => import('./views/EditTask.vue')
    }
  ]
})
