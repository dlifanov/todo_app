### Installation

Install the dependencies and start the server.

```sh
$ git clone git@bitbucket.org:dlifanov/todo_app.git
$ cd todo_app
$ yarn install
$ yarn serve
```
